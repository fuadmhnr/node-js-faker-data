const createCsvWriter = require('csv-writer').createObjectCsvWriter;
const { fa } = require('@faker-js/faker');
const casual = require('casual');

function generateRandomDate() {
  const startDate = new Date(2020, 0, 1); // Adjust the start date as needed
  const endDate = new Date(); // Use current date as end date or adjust as needed
  const randomTime = startDate.getTime() + Math.random() * (endDate.getTime() - startDate.getTime());
  const randomDate = new Date(randomTime);
  return randomDate.toISOString().split('T')[0]; // Format as "YYYY-MM-DD"
}

function generateRandomFullDate() {
  const startDate = new Date(2020, 0, 1); // Adjust the start date as needed
  const endDate = new Date(); // Use current date as end date or adjust as needed
  const randomTime = startDate.getTime() + Math.random() * (endDate.getTime() - startDate.getTime());
  const randomDate = new Date(randomTime);

  const formattedDate = randomDate.toISOString().replace('T', ' ').slice(0, -1); // Format as "YYYY-MM-DD HH:mm:ss.sss"
  return formattedDate;
}

function generateFakeData(count) {
  const data = [];
  let index = 1;

  const akad = ['00', '99'];
  const jenisPenggunaan = ['1', '3'];
  const kodePenyelenggara = ['820152', '820150', '820144', '820141', '820136', '820129', '820127', '820112', '820106', '820092', '820089', '820088', '820086', '820085', '820070', '820067', '820066', '820057', '820056', '820055', '820054', '820046', '820043', '820041', '820034', '820020', '820014', '820007', '810161', '810159', '810153', '810151', '810149', '810148', '810146', '810145', '810135', '810132', '810126', '810125', '810121', '810119', '810115', '810114', '810110', '810109', '810107', '810103', '810101', '810099', '810097', '810094', '810093', '810091', '810090', '810081', '810078', '810077', '810076', '810074', '810073', '810071', '810069', '810068', '810065', '810063', '810062', '810061', '810059', '810052', '810051', '810049', '810048', '810047', '810045', '810044', '810040', '810039', '810038', '810036', '810033', '810031', '810030', '810029', '810028', '810027', '810025', '810024', '810022', '810018', '810017', '810016', '810013', '810011', '810010', '810009', '810008', '810006', '810005', '810004', '810003', '810001']

  for (let i = 0; i < count; i++) {
    const formattedIndex = index.toString().padStart(4, '0');
    const randomAkadIndex = Math.floor(Math.random() * akad.length);
    const randomJenisPenggunaanIndex = Math.floor(Math.random() * jenisPenggunaan.length);
    const randomKodePenyelenggaraIndex = Math.floor(Math.random() * kodePenyelenggara.length);
    const fakeObject = {
      kode_penyelenggara: kodePenyelenggara[randomKodePenyelenggaraIndex],
      kode_produk_sistem: `AA${formattedIndex}`,
      nama_produk:casual.title,
      kode_akad_pendanaan: akad[randomAkadIndex],
      kode_jenis_penggunaan: jenisPenggunaan[randomJenisPenggunaanIndex],
      is_bermitra: 'TRUE',
      is_ljk: 'TRUE',
      nama_mitra: casual.full_name,
      no_surat_perjanjian_kerjasama: "1009010",
      teknologi_yang_digunakan: 'MySQL',
      tanggal_awal_perjanjian: generateRandomDate(),
      tanggal_akhir_perjanjian: generateRandomDate(),
      tanggal_waktu_launching_produk: generateRandomFullDate(),
      tanggal_waktu_penutupan_produk: generateRandomFullDate(),
      is_aktif: 'TRUE',
      keterangan: "Submitted",
    };

    data.push(fakeObject);

    index++;
    if (index > 100) {
      index = 1;
    }
  }

  return data;
}

const numberOfItems = 5000;
const fakeData = generateFakeData(numberOfItems);

const csvWriter = createCsvWriter({
  path: 'produk_pendanaan.csv',
  header: [
    { id: 'kode_penyelenggara', title: 'kode_penyelenggara' },
    { id: 'kode_produk_sistem', title: 'kode_produk_sistem' },
    { id: 'nama_produk', title: 'nama_produk' },
    { id: 'kode_akad_pendanaan', title: 'kode_akad_pendanaan' },
    { id: 'kode_jenis_penggunaan', title: 'kode_jenis_penggunaan' },
    { id: 'is_bermitra', title: 'is_bermitra' },
    { id: 'is_ljk', title: 'is_ljk' },
    { id: 'nama_mitra', title: 'nama_mitra' },
    { id: 'no_surat_perjanjian_kerjasama', title: 'no_surat_perjanjian_kerjasama' },
    { id: 'teknologi_yang_digunakan', title: 'teknologi_yang_digunakan' },
    { id: 'tanggal_awal_perjanjian', title: 'tanggal_awal_perjanjian' },
    { id: 'tanggal_akhir_perjanjian', title: 'tanggal_akhir_perjanjian' },
    { id: 'tanggal_waktu_launching_produk', title: 'tanggal_waktu_launching_produk' },
    { id: 'tanggal_waktu_penutupan_produk', title: 'tanggal_waktu_penutupan_produk' },
    { id: 'is_aktif', title: 'is_aktif' },
    { id: 'keterangan', title: 'keterangan' },
  ]
});

csvWriter.writeRecords(fakeData)
  .then(() => {
    console.log('CSV file has been written successfully');
  })
  .catch(error => {
    console.error('Error writing CSV:', error);
  });